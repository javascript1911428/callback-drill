const fs = require('fs');
const path = require('path');

function createRandomJson(err, absolutePathOfRandomDirectory, randomNumberOfFiles, callback, callback2) {
  if (err) {
    callback(err)
  }
  else {
    const filePaths = [];

    let fileCreated = 0;

    for (let index = 1; index <= randomNumberOfFiles; index++) {
      // fileNumbers.push(index);
      let filename = `file${index}.json`;
      let filepath = path.join(absolutePathOfRandomDirectory, filename);
      let fileData = JSON.stringify({ random: "Rama Krishna" }, null, 2);
      fs.writeFile(filepath, fileData, (err) => {
        if (err) {
          callback(err);
        } else {
          fileCreated += 1;

          filePaths.push(filepath)

          if (fileCreated === randomNumberOfFiles) {
            console.log("condition met");
            callback(null, filePaths, callback2);
          }
        }
      });
    }
  }
}

function createdFile(absolutePathOfRandomDirectory, randomNumberOfFiles, callback, callback2, callback3) {
  fs.mkdir(absolutePathOfRandomDirectory, (err) => {
    if (err) {
      callback(err);
    } else {
      callback(null, absolutePathOfRandomDirectory, randomNumberOfFiles, callback2, callback3)
    }
  });
}

function deleteFile(err, filepaths, callback) {
  if (err) {
    callback(err)
  }
  else {
    // fs.readdir(directoryPath, (err, files) => {
    //   if (err) {
    //     callback(err);
    //   }

    //   console.log(files);

    //   let deletedFile = 0;
    //   if (files.length === 0) {
    //     callback(null);
    //   }

    //   function deleteNextFile() {
    //     const file = files[deletedFile];
    //     const filePath = path.join(directoryPath, file);
    //     fs.unlink(filePath, (err) => {
    //       if (err) {
    //         callback(err);
    //       } else {
    //         deletedFile += 1;
    //         console.log(deleteFile);
    //         if (deletedFile === files.length) {
    //           console.log("conndition met");
    //           callback(null);
    //         } else {
    //           deleteNextFile();
    //         }
    //       }
    //     });
    //   }

    //   deleteNextFile();

    const deletedFiles = []

    let deletedFile = 0;

    for(let file=1; file<=filepaths.length; file++){

      fs.unlink(filepaths[file-1], (err) => {
        if(err){
          callback(err)
        }
        else{
          deletedFiles.push(filepaths[file-1])
          deletedFile++

          if(deleteFile == filepaths.length){
            callback(null, [filepaths,deletedFiles])
          }
        }
      })

    }
  }
}

function fsproblem1(absolutePathOfRandomDirectory, randomNumberOfFiles) {
  createdFile(absolutePathOfRandomDirectory, randomNumberOfFiles, createRandomJson, deleteFile, function(err, data){
    if(err){
      console.log(err.message)
    }
    else{
      for(let val=0; val < data.length; val++ ){
        console.log(data[val])
      }
    }
  })
  
}

module.exports = fsproblem1;