function fsProblem2() {

  const fs = require('fs');

  // Function to read the file asynchronously
  function readFileAsync(filename, callback) {
    fs.readFile(filename, 'utf8', (err, data) => {
      if (err) {
        callback(err);
      } else {
        callback(null, data);
      }
    });
  }

  // Function to write data to a file asynchronously
  function writeFileAsync(filename, data, callback) {
    fs.writeFile(filename, data, 'utf8', (err) => {
      if (err) {
        callback(err);
      } else {
        callback(null);
      }
    });
  }

  // Function to convert content to uppercase
  function convertToUpperCase(content) {
    return content.toUpperCase();
  }

  // Function to convert content to lowercase and split into sentences
  function convertToLowerCaseAndSplit(content) {
    const lowercaseContent = content.toLowerCase();
    return lowercaseContent.split(/[.!?]\s+/);
  }

  // Task 1: Read the given file lipsum.txt
  const inputFileName = '/home/rama/Downloads/lipsum.txt';

  readFileAsync(inputFileName, (err, data) => {
    if (err) {
      console.error('Error reading the file:', err);
    } else {
      // Task 2: Convert the content to uppercase & write to a new file
      const uppercaseContent = convertToUpperCase(data);
      const newUppercaseFileName = 'uppercase.txt';
      writeFileAsync(newUppercaseFileName, uppercaseContent, (err) => {
        if (err) {
          console.error('Error writing to uppercase file:', err);
        } else {
          console.log('Uppercase content written to', newUppercaseFileName);

          // Task 3: Read the new file, convert to lowercase, split into sentences & write to a new file
          const lowercaseAndSplitContent = convertToLowerCaseAndSplit(uppercaseContent);
          const newLowercaseSplitFileName = 'lowercase_split.txt';
          writeFileAsync(newLowercaseSplitFileName, lowercaseAndSplitContent.join('\n'), (err) => {
            if (err) {
              console.error('Error writing to lowercase split file:', err);
            } else {
              console.log('Lowercase split content written to', newLowercaseSplitFileName);

              // Task 4: Read the new files, sort the content & write to a new file
              const filenames = [newUppercaseFileName, newLowercaseSplitFileName];
              const sortedContent = filenames
                .map((filename) => fs.readFileSync(filename, 'utf8'))
                .sort()
                .join('\n');
              const newSortedFileName = 'sorted.txt';
              writeFileAsync(newSortedFileName, sortedContent, (err) => {
                if (err) {
                  console.error('Error writing to sorted file:', err);
                } else {
                  console.log('Sorted content written to', newSortedFileName);

                  // Task 5: Read the contents of filenames.txt and delete all new files mentioned
                  const filenamesContent = filenames.join('\n');
                  const filenamesFileName = 'filenames.txt';
                  writeFileAsync(filenamesFileName, filenamesContent, (err) => {
                    if (err) {
                      console.error('Error writing to filenames file:', err);
                    } else {
                      console.log('Filenames written to', filenamesFileName);

                      // Read and delete new files mentioned in filenames.txt
                      readFileAsync(filenamesFileName, (err, data) => {
                        if (err) {
                          console.error('Error reading filenames file:', err);
                        } else {
                          const filesToDelete = data.split('\n').filter(Boolean);
                          filesToDelete.forEach((file) => {
                            fs.unlink(file, (err) => {
                              if (err) {
                                console.error('Error deleting file:', err);
                              } else {
                                console.log('File deleted:', file);
                              }
                            });
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });

}

module.exports = fsProblem2;